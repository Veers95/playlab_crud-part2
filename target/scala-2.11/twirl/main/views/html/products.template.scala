
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object products_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class products extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[models.Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(products: List[models.Product]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<!-- Pass page title and content """),format.raw/*3.34*/("""{"""),format.raw/*3.35*/("""html between braces"""),format.raw/*3.54*/("""}"""),format.raw/*3.55*/(""" """),format.raw/*3.56*/("""to the main view -->
"""),_display_(/*4.2*/main("Products")/*4.18*/ {_display_(Seq[Any](format.raw/*4.20*/("""
  """),format.raw/*5.3*/("""<!-- HTML content for the index view -->
  <div class="row">
      <div class="col-sm-2">
        <div class="well">
          <h4>Categories</h4>
        </div>
      </div>
      <div class="col-sm-10">

        """),_display_(/*14.10*/if(flash.containsKey("success"))/*14.42*/{_display_(Seq[Any](format.raw/*14.43*/("""
          """),format.raw/*15.11*/("""<div class="alert alert-success">
            """),_display_(/*16.14*/flash/*16.19*/.get("success")),format.raw/*16.34*/("""
            """),format.raw/*17.13*/("""</div>
          """)))}),format.raw/*18.12*/("""

        """),format.raw/*20.9*/("""<table class="table table-bordered table-hover table-condensed">
          <thead>
          <!-- The header row-->
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Stock</th>
            <th>Price</th>
          </tr>
          </thead>
          <tbody>
            <!-- Product row(s) -->
					<!-- Start of For loop - For each p in products add a row -->
					"""),_display_(/*34.7*/for(p <- products) yield /*34.25*/ {_display_(Seq[Any](format.raw/*34.27*/("""
					"""),format.raw/*35.6*/("""<tr>
              <td>"""),_display_(/*36.20*/p/*36.21*/.getId),format.raw/*36.27*/("""</td>
              <td>"""),_display_(/*37.20*/p/*37.21*/.getName),format.raw/*37.29*/("""</td>
              <td>"""),_display_(/*38.20*/p/*38.21*/.getDescription),format.raw/*38.36*/("""</td>
              <td>"""),_display_(/*39.20*/p/*39.21*/.getStock),format.raw/*39.30*/("""</td>
              <td>&euro; """),_display_(/*40.27*/("%.2f".format(p.getPrice))),format.raw/*40.54*/("""</td>
					</tr>
					""")))}),format.raw/*42.7*/(""" """),format.raw/*42.8*/("""<!-- End of For loop -->
          </tbody>
        </table>
        <p>
          <a href=""""),_display_(/*46.21*/routes/*46.27*/.HomeController.addProduct()),format.raw/*46.55*/("""">
            <button class="btn btn-primary">Add a Product</button>
            </a>
          </p>
      </div>
  </div>
  <!-- End of content for main -->
""")))}))
      }
    }
  }

  def render(products:List[models.Product]): play.twirl.api.HtmlFormat.Appendable = apply(products)

  def f:((List[models.Product]) => play.twirl.api.HtmlFormat.Appendable) = (products) => apply(products)

  def ref: this.type = this

}


}

/**/
object products extends products_Scope0.products
              /*
                  -- GENERATED --
                  DATE: Thu Nov 24 16:16:55 GMT 2016
                  SOURCE: /home/wdd/webapps/playlab_crud-part1/app/views/products.scala.html
                  HASH: f97c2838fcc19f77ffb0f6d9ce95d97b030d2440
                  MATRIX: 765->1|892->33|920->35|980->68|1008->69|1054->88|1082->89|1110->90|1157->112|1181->128|1220->130|1249->133|1491->348|1532->380|1571->381|1610->392|1684->439|1698->444|1734->459|1775->472|1824->490|1861->500|2317->930|2351->948|2391->950|2424->956|2475->980|2485->981|2512->987|2564->1012|2574->1013|2603->1021|2655->1046|2665->1047|2701->1062|2753->1087|2763->1088|2793->1097|2852->1129|2900->1156|2953->1179|2981->1180|3101->1273|3116->1279|3165->1307
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|34->3|34->3|35->4|35->4|35->4|36->5|45->14|45->14|45->14|46->15|47->16|47->16|47->16|48->17|49->18|51->20|65->34|65->34|65->34|66->35|67->36|67->36|67->36|68->37|68->37|68->37|69->38|69->38|69->38|70->39|70->39|70->39|71->40|71->40|73->42|73->42|77->46|77->46|77->46
                  -- GENERATED --
              */
          