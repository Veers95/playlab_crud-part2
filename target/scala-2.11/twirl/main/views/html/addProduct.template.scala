
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addProduct_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object addProduct_Scope1 {
import helper._

class addProduct extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[models.Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(addProductForm: Form[models.Product]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.40*/("""

"""),_display_(/*6.2*/main("Add Product")/*6.21*/ {_display_(Seq[Any](format.raw/*6.23*/("""

  """),format.raw/*8.3*/("""<h3>Add a new Product</h3>

    """),_display_(/*10.6*/form(action = routes.HomeController.addProductSubmit(), 'class -> "form=horizontal", 'role->"form")/*10.105*/ {_display_(Seq[Any](format.raw/*10.107*/("""


      """),_display_(/*13.8*/inputText(addProductForm("name"), '_label -> "Name", 'class -> "form-control")),format.raw/*13.86*/("""
      """),_display_(/*14.8*/inputText(addProductForm("description"), '_label -> "Description", 'class -> "form-control")),format.raw/*14.100*/("""
      """),_display_(/*15.8*/inputText(addProductForm("stock"), '_label -> "Stock", 'class -> "form-control")),format.raw/*15.88*/("""
      """),_display_(/*16.8*/inputText(addProductForm("price"), '_label -> "Price", 'class -> "form-cotrol")),format.raw/*16.87*/("""


      """),format.raw/*19.7*/("""<div class="actions">
          <input type="submit" value="Add Product" class="btn btn-primary">
                  <a href=""""),_display_(/*21.29*/routes/*21.35*/.HomeController.index()),format.raw/*21.58*/(""""
                    <button class="btn btn-warning">Cancel</button>
                  </a>
            </div>
            
        """)))}),format.raw/*26.10*/("""

    """)))}))
      }
    }
  }

  def render(addProductForm:Form[models.Product]): play.twirl.api.HtmlFormat.Appendable = apply(addProductForm)

  def f:((Form[models.Product]) => play.twirl.api.HtmlFormat.Appendable) = (addProductForm) => apply(addProductForm)

  def ref: this.type = this

}


}
}

/**/
object addProduct extends addProduct_Scope0.addProduct_Scope1.addProduct
              /*
                  -- GENERATED --
                  DATE: Thu Nov 24 16:19:09 GMT 2016
                  SOURCE: /home/wdd/webapps/playlab_crud-part1/app/views/addProduct.scala.html
                  HASH: 37292d1888fc6d2290dbf4b0b4e71ebfb8d5f2bb
                  MATRIX: 818->20|951->58|979->61|1006->80|1045->82|1075->86|1134->119|1243->218|1284->220|1320->230|1419->308|1453->316|1567->408|1601->416|1702->496|1736->504|1836->583|1872->592|2025->718|2040->724|2084->747|2249->881
                  LINES: 30->4|35->4|37->6|37->6|37->6|39->8|41->10|41->10|41->10|44->13|44->13|45->14|45->14|46->15|46->15|47->16|47->16|50->19|52->21|52->21|52->21|57->26
                  -- GENERATED --
              */
          